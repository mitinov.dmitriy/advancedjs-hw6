const outputField = document.querySelector("#info");
const searchBtn = document.querySelector("#search");

class Display {
    constructor({continent, country, regionName, city, district}) {
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
    }
    displayInfo(elemToAppend){
        if (this.continent.trim()){
            elemToAppend.value += `Client continent: ${this.continent}\n`;
        }
        if (this.country.trim()){
            elemToAppend.value += `Client country: ${this.country}\n`;
        }
        if (this.regionName.trim()){
            elemToAppend.value += `Client region: ${this.regionName}\n`;
        }
        if (this.city.trim()){
            elemToAppend.value += `Client city: ${this.city}\n`;
        }
        if (this.district.trim()){
            elemToAppend.value += `Client district: ${this.district}\n`;
        }
    }
}

class IpLocator {
    constructor(url="https://api.ipify.org/?format=json") {
        this.url = url;
    }
    async _getIpAddressInfo(){
        try {
            const response = await fetch(this.url);
            const jsonResponse = await response.json();
            if (jsonResponse["ip"]){
                return jsonResponse["ip"];
            }
        }
        catch (e) {
            console.error(e.message);
        }
    }
    showIpInformation(){
        this._getIpAddressInfo().then(async (address)=>{
            try {
                const response = await fetch("http://ip-api.com/json/" + address + "?fields=continent,country,regionName,city,district");
                const data = await response.json();
                const clientInfo = new Display(data);
                clientInfo.displayInfo(outputField);
            }
            catch (err){
                console.error(err.message);
            }
        });
    }
}

const clientIp = new IpLocator();
searchBtn.addEventListener("click", ()=>{
    clientIp.showIpInformation();
})